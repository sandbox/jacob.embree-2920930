<?php

/**
 * @file
 * Google Measurement Protocol module.
 */

/**
 * Create a payload that will go to Google's collection service.
 *
 * Ideally, you wouldn't use this function, but you'd use one of the many
 * helper functions that are better suited to their individual tasks.
 *
 * @param array $data
 *   An array containing one or more data keys. See the functions below for
 *   more information on how to use them.
 *
 * @see mptph()
 * @see mpte()
 *
 * @return array
 *   The prepared payload, ready to be sent to Google. If NULL is returned,
 *   then the account was not set.
 */
function measurement_protocol_create_payload($data) {

  // Grab this from the googleanalytics module.
  $payload['tid'] = variable_get('googleanalytics_account', '');

  if ($payload['tid'] === '') {
    return NULL;
  }

  $payload['v'] = 1;

  if ($GLOBALS['user']->uid) {
    $payload['uid'] = $GLOBALS['user']->uid;
  }
  $payload['cid'] = measurement_protocol_get_cid();

  return array_merge($payload, $data);
}

/**
 * Sends an action to Google's collection service.
 *
 * Returns the response that drupal_http_request() returns.
 *
 * @param array $data
 *   A data array containing a prepared payload.
 *
 * @return object
 *   The response from drupal_http_request().
 */
function measurement_protocol_send_action($data) {
  if ($data !== NULL) {
    // We pass the '&' because on some servers it defaults to '&amp;'.
    $request_data = http_build_query($data, '', '&');
    return drupal_http_request('https://www.google-analytics.com/collect', array(
      'method' => 'POST',
      'data' => $request_data,
    ));
  }
}

/**
 * Builds and sends an MP payload.
 *
 * Takes data as its parameter. Builds a GA payload and then sends the data
 * to the collection service. While you could use this directly, it is
 * recommended to use helper functions.
 *
 * @param array $data
 *    An associative array containing keys with one or more data points to send
 *    to Google.
 */
function mpt($data) {
  $data = measurement_protocol_create_payload($data);
  $response = measurement_protocol_send_action($data);
  if ($response->code != 200) {
    watchdog('measurement_protocol', 'Error (%code): %error', array(
      '%code' => $response->code,
      '%error' => $response->error,
    ), WATCHDOG_ERROR);
  }
}

/**
 * Tracks a page hit with Google Analytics.
 *
 * @param string $title
 *   The title of the page.
 * @param string $referrer
 *   The referring URL.
 * @param string path
 *   Path to the page tracked.
 * @param array $data
 *   (optional) Extra data to send with the event.
 */
function mptph($title, $referrer, $path, $data = array()) {
  $data['t'] = 'pageview';
  $data['dt'] = $title;
  $data['dr'] = $referrer;
  $data['dp'] = $path;
  $data['dh'] = parse_url($GLOBALS['base_url'], PHP_URL_HOST);
  mpt($data);
}

/**
 * Tracks an event with Google Analytics.
 *
 * @param string $category
 *   The event category.
 * @param string $action
 *   The event action.
 * @param string $label
 *   (optional) The event label.
 * @param int $value
 *   (optional) The event value.
 * @param array $data
 *   (optional) Extra data to send with the event.
 */
function mpte($category, $action, $label = NULL, $value = NULL, $data = array()) {
  $data['t'] = 'event';
  $data['ec'] = $category;
  $data['ea'] = $action;
  if (isset($label)) {
    $data['el'] = $label;
  }
  if (isset($value)) {
    $data['ev'] = $value;
  }
  mpt($data);
}

/**
 * Tracks a social interaction with Google Analytics.
 *
 * @param string $network
 *   The social network.
 * @param string $action
 *   The social action, for example, "like".
 * @param string $target
 *   The target of the social interaction.
 * @param array $data
 *   (optional) Extra data to send with the interaction.
 */
function mptsi($network, $action, $target, $data = array()) {
  $data['t'] = 'social';
  $data['sn'] = $network;
  $data['sa'] = $action;
  $data['st'] = $target;
  mpt($data);
}

/**
 * Returns a client ID to be used for the cid parameter.
 *
 * If the _ga cookie is available then use it.  If it is not available, then we
 * generate our own UUID or pass the anonymous CID.
 *
 * @return
 *   An ID suitable for the cid parameter.
 */
function measurement_protocol_get_cid() {
  if (isset($_COOKIE['_ga'])) {
    $_ga_parts = explode(".", $_COOKIE['_ga']);
    return $_ga_parts[2] . '.' . $_ga_parts[3];
  }
  else {
    if (variable_get('measurement_protocol_uuid_anon', 'anon') == 'anon') {
      // Return the Google Analytics anonymous client ID.
      return 555;
    }
    else {
      // Generate a UUID.
      measurement_protocol_gen_uuid();
    }
  }
}

/**
 * Generates a UUID (version 4).
 *
 * @return string
 *   A string containing a UUID.
 *
 * @see http://www.php.net/manual/en/function.uniqid.php#94959
 */
function measurement_protocol_gen_uuid() {
  return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
    // 32 bits for "time_low".
    mt_rand(0, 0xffff), mt_rand(0, 0xffff),
    // 16 bits for "time_mid".
    mt_rand(0, 0xffff),
    // 16 bits for "time_hi_and_version".
    // four most significant bits holds version number 4.
    mt_rand(0, 0x0fff) | 0x4000,
    // 16 bits, 8 bits for "clk_seq_hi_res".
    // 8 bits for "clk_seq_low".
    // two most significant bits holds zero and one for variant DCE1.1.
    mt_rand(0, 0x3fff) | 0x8000,
    // 48 bits for "node".
    mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
  );
}
